function(messageList list)
  set(listP "")
  foreach(element ${list})
    set(listP "${listP}\n${element}")
  endforeach()
  message(STATUS ${listP})
endfunction()

file(
  STRINGS ${LOG_FILE_NAME} errors
  REGEX "LaTeX Error")
file(
  STRINGS ${LOG_FILE_NAME} warnings
  REGEX "LaTeX Warning:")
list(LENGTH errors errorCount)
list(LENGTH warnings warningCount)
if(errorCount GREATER 0 OR warningCount GREATER 0)
  messageList("${errors}")
  messageList("${warnings}")
endif()
